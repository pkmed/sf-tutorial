<?php

namespace App\Event;

use App\Entity\User;
use Symfony\Component\EventDispatcher\GenericEvent;

class UserRegisterEvent extends GenericEvent
{
    const NAME='user.register';
    /**
     * @var User
     */
    public $registeredUser;

    public function __construct(User $registeredUser)
    {
        $this->registeredUser = $registeredUser;
    }

    /**
     * @return User
     */
    public function getRegisteredUser(): User
    {
        return $this->registeredUser;
    }
}