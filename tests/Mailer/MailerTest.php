<?php

namespace App\Tests\Mailer;

use App\Entity\User;
use App\Mailer\Mailer;
use PHPUnit\Framework\TestCase;

class MailerTest extends TestCase
{
    public function testConfirmationEmail()
    {
        $user = new User();
        $user->setEmail('john@doe.com');

        $swiftMailer = $this->getMockBuilder(\Swift_Mailer::class)
            ->disableOriginalConstructor()
            ->getMock();
        $swiftMailer->expects($this->once())->method('send')
        ->with($this->callback(function($subject){
            $messageString = (string)$subject;

            return strpos($messageString, 'From: me@domail.com') !== false
                && strpos($messageString, 'Content-Type: text/html; charset=utf-8') !== false
                && strpos($messageString, 'Subject: welcome to the micro-post app') !== false
                && strpos($messageString, 'To: john@doe.com') !== false
                && strpos($messageString, 'This is a message body') !== false;
        }));

        $twigMock = $this->getMockBuilder(\Twig\Environment::class)
            ->disableOriginalConstructor()
            ->getMock();
        $twigMock->expects($this->once())
            ->method('render')
            ->with(
                'email/registration.html.twig',
                [
                    'user' => $user,
                ]
            )->willReturn('This is a message body');

        $mailer = new Mailer($swiftMailer, $twigMock, 'me@domail.com');
        $mailer->sendConfirmationEmail($user);
    }
}